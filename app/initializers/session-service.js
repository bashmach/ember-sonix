export function initialize(container, application) {
  application.inject('route', 'session', 'service:session');
  application.inject('controller:application', 'session', 'service:session');

  application.register('service:session-check', function beforeModel(transition) {
    const isLoggedIn = this.session.get('isLoggedIn');

    if (!isLoggedIn) {
      this.session.set('attemptedTransition', transition);
      this.transitionTo('login');
    }
  }, {instantiate: false});

  const routes = ['track', 'track/new'];

  routes.forEach((route) => {
    application.inject('route:' + route, 'beforeModel', 'service:session-check')
  });
}

export default {
  name: 'session-service',
  initialize: initialize
};
