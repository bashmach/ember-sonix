import Ember from 'ember';

export default Ember.Object.extend({
  username: null,
  attemptedTransition: null,
  isLoggedIn: Ember.computed.notEmpty('username')
});
