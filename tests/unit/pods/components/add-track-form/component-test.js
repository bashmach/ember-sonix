import {
  moduleForComponent,
  test
} from 'ember-qunit';

moduleForComponent('add-track-form', {
  // specify the other units that are required for this test
  // needs: ['component:foo', 'helper:bar']
});

test('it renders', function(assert) {
  assert.expect(2);

  // creates the component instance
  var component = this.subject();
  assert.equal(component._state, 'preRender');

  component.set('title', 'test title');
  component.set('artist', 'artist name lower case');

  assert.equals(component.get('artistStartsWithUpperCase'), function() {
    return true;
  });

  // renders the component to the page
  this.render();
  assert.equal(component._state, 'inDOM');
});
